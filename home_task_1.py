# Напишіть декоратор, який буде для переданої функції заміряти час виконання.
# Напишіть програму яка буде виводити 25 перших чисел Фібоначі, використовуючи
# для цього три наведені в тексті заняття функції - без кеша, з кешем довільної
# довжини, з кешем з модулю functools з максимальною кількістю 10 елементів.
# За допомогою написаного Вами декоратора заміряйте і порівняйте швидкість 
# роботи ціх трьох варіантів.

import functools
import time
from typing import Callable, Any


def timer(func: Callable) -> Any:
    """Print the runtime of the decorated function."""
    @functools.wraps(func)
    def wrapper_timer(*args, **kwargs):
        start_time = time.perf_counter()
        value = func(*args, **kwargs)
        end_time = time.perf_counter()
        runtime = end_time - start_time
        print(f"Done! {func.__name__} in {runtime} secs")
        return value

    return wrapper_timer



@timer
def long_loop(num_times):
    for _ in range(num_times):
        sum([i**3 for i in range(1000000)])
